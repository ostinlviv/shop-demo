import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import Shop from './components/Shop';
import Layout from './hoc/layout';
import Auth from './hoc/auth';
import RegisterLogin from './components/RegisterLogin';
import Register from './components/RegisterLogin/register';
import ProductPage from './components/Product';
import UserDashboard from './components/User';
import AddProduct from './components/User/Admin/addProduct';
import ManageCategories from './components/User/Admin/manageCategories';
import UserCart from './components/User/cart';
import UpdateProfile from './components/User/updateProfile';
import ManageSite from './components/User/Admin/manageSite';
import ResetUser from './components/ResetUser';
import ResetPass from './components/ResetUser/resetPass';
import AddFile from './components/User/Admin/addFile';
import PageNotFound from './components/utils/pageNotFound';

const Routes = () => {
  return (
    <Layout>
      <Switch>
        <Route path="/user/dashboard" exact component={Auth(UserDashboard, true)} />
        <Route path="/user/cart" exact component={Auth(UserCart, true)} />
        <Route path="/user/user_profile" exact component={Auth(UpdateProfile, true)} />
        <Route path="/admin/add_product" exact component={Auth(AddProduct, true)} />
        <Route path="/admin/manage_categories" exact component={Auth(ManageCategories, true)} />
        <Route path="/admin/site_info" exact component={Auth(ManageSite, true)} />
        <Route path="/admin/add_file" exact component={Auth(AddFile, true)} />

        <Route path="/reset_password/:token" exact component={Auth(ResetPass, false)} />
        <Route path="/reset_user" exact component={Auth(ResetUser, false)} />
        <Route path="/product_detail/:id" exact component={Auth(ProductPage, null)} />
        <Route path="/register" exact component={Auth(Register, false)} />
        <Route path="/login" exact component={Auth(RegisterLogin, false)} />
        <Route path="/shop" exact component={Auth(Shop, null)} />
        <Route path="/" exact component={Auth(Home, null)} />

        <Route component={Auth(PageNotFound)} />
      </Switch>
    </Layout>    
  )
}

export default Routes;
