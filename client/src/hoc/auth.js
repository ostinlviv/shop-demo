import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { auth } from '../store/actions/userActions';
import CircularProgress from '@material-ui/core/CircularProgress';

export default function(ComposedClass, reload, adminRoute = null) {
    const AuthenticationCheck = (props) => {
        const [loading, setLoading] = useState(true);

        useEffect(() => {
            props.dispatch(auth()).then(response => {
                let user = response.payload;
                if(!user.isAuth) {
                    if(reload) {
                        props.history.push('/login');
                    }
                } else {
                    if(adminRoute && !user.isAdmin) {
                        props.history.push('/user/dashboard');
                    } else {
                        if(reload === false) {
                            props.history.push('/user/dashboard');
                        }
                    }
                }
                setLoading(false);
            })
        }, []);
        if(loading){
            return (
                <div className="main_loader">
                    <CircularProgress style={{color: '#2196F3'}} thickness={7} />
                </div>                    
            );
        }
        return (
            <ComposedClass {...props} user={props} />
        );
    }

    function mapStateToProps(state) {
        return {
            user: state.user
        }
    }

    return connect(mapStateToProps)(AuthenticationCheck);
}