import React, { useEffect, useState } from 'react';

import Header from '../components/HeaderFooter/Header';
import Footer from '../components/HeaderFooter/Footer';

import { connect } from 'react-redux';
import { fetchSiteData } from '../store/actions/siteActions';

const Layout = (props) => {
    const [data, setData] = useState({});
    useEffect(() => {
        props.onFetchSiteData();
    }, []);
    useEffect(() => {
        if (props.site && props.site.siteData && props.site.siteData[0]) {
            setData(props.site)
        }
    }, [props.site]);
    return (
        <div>
            <Header />
            <div className="page_container">
                {props.children}
            </div>
            <Footer data={data}/>
        </div>
    )
    
};

const mapStateToProps = state => {
    return {
        site: state.site
    }
}

const mapDispatchToProps = dispatch => {
    return {
      onFetchSiteData: () =>
        dispatch(fetchSiteData())
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
