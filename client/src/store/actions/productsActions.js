import axios from 'axios';
import {
    GET_PRODUCTS_BY_SELL,
    GET_PRODUCTS_BY_ARRIVAL,
    GET_BRANDS,
    ADD_BRAND,
    GET_OPTIONS,
    ADD_OPTION,
    GET_PRODUCTS_TO_SHOP,
    ADD_PRODUCT,
    CLEAR_PRODUCT,
    GET_PRODUCT_DETAIL,
    CLEAR_PRODUCT_DETAIL
} from './types';

import { PRODUCT_SERVER } from '../../components/utils/misc';

export function getProductDetail(id) {
    const request = axios.get(`${PRODUCT_SERVER}/product?id=${id}&type=single`)
        .then(response => {
            return response.data.data[0];
        });

    return {
        type: GET_PRODUCT_DETAIL,
        payload: request
    }
}

export function clearProductDetail(id) {
    return {
        type: CLEAR_PRODUCT_DETAIL,
        payload: ''
    }
}

export function getProductsBySell() {
    const request = axios.get(`${PRODUCT_SERVER}?sortBy=sold&order=desc&limit=4`)
        .then(response => response.data);

    return {
        type: GET_PRODUCTS_BY_SELL,
        payload: request
    }
}

export function getProductsByArrival() {
    const request = axios.get(`${PRODUCT_SERVER}?sortBy=created&order=desc&limit=4`)
        .then(response => response.data);

    return {
        type: GET_PRODUCTS_BY_ARRIVAL,
        payload: request
    }
}

export function getBrands() {
    const request = axios.get(`${PRODUCT_SERVER}/brands`)
        .then(response => response.data);

    return {
        type: GET_BRANDS,
        payload: request
    }
}

export function addBrand(dataToSubmit, existingBrands) {
    const request = axios.post(`${PRODUCT_SERVER}/brands`, dataToSubmit)
        .then(response => {
            let brands = [
                ...existingBrands,
                response.data.data
            ];
            return {
                success: response.data.success,
                brands
            }
        });

    return {
        type: ADD_BRAND,
        payload: request
    }
}

export function getOptions() {
    const request = axios.get(`${PRODUCT_SERVER}/options`)
        .then(response => response.data);

    return {
        type: GET_OPTIONS,
        payload: request
    }
}

export function addOption(dataToSubmit, existingOptions) {
    const request = axios.post(`${PRODUCT_SERVER}/options`, dataToSubmit)
        .then(response => {
            let options = [
                ...existingOptions,
                response.data.data
            ];
            return {
                success: response.data.success,
                options
            }
        });

    return {
        type: ADD_OPTION,
        payload: request
    }
}

export function getProductsToShop(skip, limit, filters = [], previousState = []) {
    const data = {
        limit,
        skip,
        filters
    }


    const request = axios.post(`${PRODUCT_SERVER}/shop`, data)
        .then(response => {
            let newState = [
                ...previousState,
                ...response.data.products
            ]
            return {
                size: response.data.size,
                products: newState
            }
        });

    return {
        type: GET_PRODUCTS_TO_SHOP,
        payload: request
    }
}

export function addProduct(dataToSubmit) {
    const request = axios.post(`${PRODUCT_SERVER}`, dataToSubmit)
        .then(response => response.data);

    return {
        type: ADD_PRODUCT,
        payload: request
    }
}

export function clearProduct() {
    return {
        type: CLEAR_PRODUCT,
        payload: ''
    }
}