import axios from 'axios';
import {
    // GET_SITE_DATA,
    FETCH_SITE_DATA_SUCCESS,
    FETCH_SITE_DATA_FAIL,
    FETCH_SITE_DATA_START,
    UPDATE_SITE_DATA
} from './types';

import { SITE_SERVER } from '../../components/utils/misc';

export const fetchSiteDataSuccess = ( data ) => {
    return {
        type: FETCH_SITE_DATA_SUCCESS,
        payload: data
    };
};

export const fetchSiteDataFail = ( error ) => {
    return {
        type: FETCH_SITE_DATA_FAIL,
        error: error
    };
};

export const fetchSiteDataStart = () => {
    return {
        type: FETCH_SITE_DATA_START
    };
};

export const fetchSiteData = () => {
    return dispatch => {
        dispatch(fetchSiteDataStart());
        axios.get(`${SITE_SERVER}`)
            .then( response => {
                dispatch(fetchSiteDataSuccess(response.data.data));
            } )
            .catch( err => {
                dispatch(fetchSiteDataFail(err));
            } );
    };
};

export const updateSiteData = (dataToSubmit) => {
    const request = axios.post(`${SITE_SERVER}`, dataToSubmit)
        .then(response => response.data);
    return {
        type: UPDATE_SITE_DATA,
        payload: request
    }
};