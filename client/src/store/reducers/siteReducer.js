import {
    // GET_SITE_DATA,
    FETCH_SITE_DATA_SUCCESS,
    FETCH_SITE_DATA_FAIL,
    FETCH_SITE_DATA_START,
    UPDATE_SITE_DATA
} from '../actions/types';

import { updateObject } from '../../shared/utility';

const initialState = {
    siteData: [],
    loading: false
};

const fetchSiteDataStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const fetchSiteDataSuccess = ( state, action ) => {
    return updateObject( state, {
        siteData: action.payload,
        loading: false
    } );
};

const fetchSiteDataFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const updateSiteData = ( state, action ) => {
    return updateObject( state, { siteData: action.payload.siteInfo } );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case FETCH_SITE_DATA_START: return fetchSiteDataStart( state, action );
        case FETCH_SITE_DATA_SUCCESS: return fetchSiteDataSuccess( state, action );
        case FETCH_SITE_DATA_FAIL: return fetchSiteDataFail( state, action );
        case UPDATE_SITE_DATA: return updateSiteData( state, action );
        default: return state;
    }
};

export default reducer;