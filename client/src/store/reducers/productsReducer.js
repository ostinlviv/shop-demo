import {
    GET_PRODUCTS_BY_SELL,
    GET_PRODUCTS_BY_ARRIVAL,
    GET_BRANDS,
    GET_OPTIONS,
    ADD_OPTION,
    GET_PRODUCTS_TO_SHOP,
    ADD_PRODUCT,
    CLEAR_PRODUCT,
    ADD_BRAND,
    GET_PRODUCT_DETAIL,
    CLEAR_PRODUCT_DETAIL
} from '../actions/types';

export default function(state={}, action) {
    switch(action.type) {
        case GET_PRODUCTS_BY_SELL:
            return { ...state, bySell: action.payload.data }
        case GET_PRODUCTS_BY_ARRIVAL:
            return { ...state, byArrival: action.payload.data }
        case GET_BRANDS:
            return { ...state, brands: action.payload.data }
        case ADD_BRAND:
            return { 
                ...state, 
                addBrand: action.payload.success,
                brands: action.payload.brands 
            }
        case GET_OPTIONS:
            return { ...state, options: action.payload.data }
        case ADD_OPTION:
            return { 
                ...state, 
                addOption: action.payload.success,
                options: action.payload.options 
            }
        case GET_PRODUCTS_TO_SHOP:
            return { 
                ...state,
                toShop: action.payload.products,
                toShopSize: action.payload.size
            }
        case ADD_PRODUCT:
            return { ...state, addProduct: action.payload }
        case CLEAR_PRODUCT:
            return { ...state, addProduct: action.payload }
        case GET_PRODUCT_DETAIL:
            return { ...state, prodDetail: action.payload }
        case CLEAR_PRODUCT_DETAIL:
            return { ...state, prodDetail: action.payload }
        
        default:
            return state;
    }
}