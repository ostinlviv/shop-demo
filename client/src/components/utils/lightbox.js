import React from 'react';
import Carousel, { Modal, ModalGateway } from 'react-images';

const ImageLightBox = (props) => {
    const getImages = () => {
        if(props.images) {
            const imagesArray = [];
            props.images.forEach(element => {
                imagesArray.push({source: `${element}`})
            });
            return imagesArray;
        }
    }

    const closeLightBox = () => {
        props.onClose();
    }

    return (
        <ModalGateway>
            {props.isOpen ? (
                <Modal onClose={() => closeLightBox()}>
                    <Carousel views={getImages()} currentIndex={props.pos} />
                </Modal>
            ) : null}
        </ModalGateway>
    )
}

export default ImageLightBox;