import React from 'react';
import PaypalExpressBtn from 'react-paypal-express-checkout';

const Paypal = (props) => {
    const onSuccess = (payment) => {
        props.onSuccess(payment);
    }
    const onCancel = (data) => {
        props.transactionCancelled(data);
    }
    const onError = (err) => {
        props.transactionError(err);
    }
    let env = 'sandbox';
    let currency = 'USD';
    let total = props.toPay;
    const client = {
        sandbox: 'AfIAVehmk1JGuU_xmmewkh1jZYtkNxwaamPH11bc0wQsgxG2-CT-cOwbgrqPK6APoI22wRRCtqYi2TcB',
        production: ''
    }
    return (
        <div>
            <PaypalExpressBtn
                env={env}
                client={client}
                currency={currency}
                total={total}
                onError={onError}
                onSuccess={onSuccess}
                onCancel={onCancel}
                style={{
                    size: 'large',
                    color: 'blue',
                    shape: 'rect',
                    label: 'checkout'
                }}
            />
        </div>
    )
}

export default Paypal;
