export const USER_SERVER = '/api/v1/users';
export const AUTH_SERVER = '/api/v1/auth';
export const PRODUCT_SERVER = '/api/v1/products';
export const SITE_SERVER = '/api/v1/site';