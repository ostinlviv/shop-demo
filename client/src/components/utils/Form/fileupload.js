import React, { useState } from 'react';
import Dropzone from 'react-dropzone';
import axios from 'axios';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import faPlusCircle from '@fortawesome/fontawesome-free-solid/faPlusCircle';
import CircularProgress from '@material-ui/core/CircularProgress';

const Fileupload = (props) => {
    const [uploadedFiles, setUploadedFiles] = useState([]);
    const [uploading, setUploading] = useState(false);

    const onDrop = files => {
        setUploading(true);
        let formData = new FormData();
        const config = {
            header: {'content-type': 'multipart/form-data'}
        }
        formData.append("file", files[0]);

        axios.post('/api/v1/users/uploadimage', formData, config)
            .then(response => {
                setUploading(false);
                setUploadedFiles([...uploadedFiles, response.data]);
                props.imagesHandler(uploadedFiles);
            })
    }

    const onRemove = (id) => {
        axios.get(`/api/v1/users/removeimage?public_id=${id}`).then(response => {
            let images = uploadedFiles.filter(item => {
                return item.public_id !== id;
            });
            setUploadedFiles(images);
            props.imagesHandler(images);
        });
    }

    const showUploadedImages = () => (
        uploadedFiles.map(item => (
            <div 
                className="dropzone_box"
                key={item.public_id}
                onClick={() => onRemove(item.public_id)}
            >
                <div
                    className="wrap"
                    style={{
                        background: `url(${item.url}) no-repeat`
                    }}
                ></div>
            </div>
        ))
    );

    if(props.reset) {
        setUploadedFiles([])
    }

    return (
        <div>
            <section>
                <div className="dropzone clear">
                    <Dropzone
                        onDrop={(e) => onDrop(e)}
                        multiple={false}                            
                    >
                        {({getRootProps, getInputProps}) => (
                            <div className="dropzone_box">
                                <div className="wrap" {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    <FontAwesomeIcon
                                        icon={faPlusCircle}
                                    />
                                </div>
                            </div>
                        )}
                    </Dropzone>
                    {showUploadedImages()}
                    {
                        uploading ?
                            <div className="dropzone_box" style={{
                                textAlign: "center",
                                paddingTop: '60px'
                            }}>
                                <CircularProgress
                                    style={{color: '#00bcd4'}}
                                    thickness={7}
                                />
                            </div>
                        : null
                    }
                </div>
            </section>
        </div>
    )
}

export default Fileupload;