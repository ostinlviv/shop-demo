import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import { logoutUser } from '../../../store/actions/userActions';

const Header = (props) => {
    const links = {
        page: [
            {
                name: 'Home',
                linkTo: '/',
                public: true
            },
            {
                name: 'Products',
                linkTo: '/shop',
                public: true
            }
        ],
        user: [
            {
                name: 'My Cart',
                linkTo: '/user/cart',
                public: false
            },
            {
                name: 'My Account',
                linkTo: '/user/dashboard',
                public: false
            },
            {
                name: 'Log in',
                linkTo: '/login',
                public: true
            },
            {
                name: 'Log out',
                linkTo: '/user/logout',
                public: false
            }
        ]
    }

    const defaultLink = (item,i) => (
        item.name === 'Log out' ?
        <div className="log_out_link" key={i} onClick={() => logoutHandler()}>{item.name}</div> :
        <Link to={item.linkTo} key={i}>
            {item.name}
        </Link>
    )

    const logoutHandler = () => {
        props.dispatch(logoutUser()).then(response => {
            if(response.payload.success) {
                props.history.push('/');
            }
        })
    }

    const cartLink = (item,i) => {
        const user = props.user.userData;
        return (
            <div className="cart_link" key={i}>
                <span>{ user.cart ? user.cart.length : 0 }</span>
                <Link to={item.linkTo}>
                    {item.name}
                </Link>
            </div>
        )
    }

    const showLinks = (type) => {
        let list = [];
        
        if(props.user.userData) {
            type.forEach((item) => {
                if(!props.user.userData.isAuth) {
                    if(item.public === true) {
                        list.push(item);
                    }
                } else {
                    if(item.name !== 'Log in') {
                        list.push(item);
                    }
                }
            })
        }
        return list.map((item,i) => {
            if(item.name !== 'My Cart') {
                return defaultLink(item,i);
            } else {
                return cartLink(item,i);
            }            
        })
    }

    
    return (
        <header className="bck_b_light">
            <div className="container">
                <div className="left">
                    <div className="logo">
                        SHOP
                    </div>
                </div>
                <div className="right">
                    <div className="top">
                        {showLinks(links.user)}
                    </div>
                    <div className="bottom">
                        {showLinks(links.page)}
                    </div>
                </div>
            </div>
        </header>
    );
    
};

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(withRouter(Header));
