import React, { useEffect } from 'react';
import HomeSlider from './homeSlider';
import HomePromotion from './homePromotion';
import CardBlock from '../utils/cardBlock';

import { connect } from 'react-redux';
import { getProductsBySell, getProductsByArrival } from '../../store/actions/productsActions';

const Home = (props) => {
    useEffect(() => {
        props.dispatch(getProductsBySell());
        props.dispatch(getProductsByArrival());
    }, []);

    return (
        <div>
            <HomeSlider/>
            <CardBlock
                list={props.products.bySell}
                title="Best selling guitars"
            />
            <HomePromotion/>
            <CardBlock
                list={props.products.byArrival}
                title="New arrivals"
            />
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
}

export default connect(mapStateToProps)(Home);
