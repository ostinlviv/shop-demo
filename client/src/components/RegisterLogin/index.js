import React from 'react';
import MyButton from '../utils/button';
import Login from './login';

const RegisterLogin = () => {
    return (
        <div className="page_wrapper">
            <div className="container">
                <div className="register_login_container">
                    <div className="left">
                        <h1>New Customers</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id massa vel neque blandit varius. Vestibulum pellentesque sem ut lorem semper fermentum. Nulla sit amet leo id lectus ultricies dignissim. Duis porttitor tincidunt nisi, ac iaculis nunc ultricies quis. Mauris sagittis ligula dolor, vel commodo massa luctus nec.</p>
                        <MyButton
                            type="default"
                            title="Create an account"
                            linkTo="/register"
                            addStyles={{
                                margin: '10px 0 0 0'
                            }}
                        />
                    </div>
                    <div className="right">
                        <h2>Registered Customers</h2>
                        <p>If you have an account please log in.</p>
                        <Login />
                    </div>
                </div>
            </div>
        </div>
    )
};

export default RegisterLogin;
