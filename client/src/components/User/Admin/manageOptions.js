import React, { useState, useEffect } from 'react';
import FormField from '../../utils/Form/formfield';
import { update, generateData, isFormValid, resetFields } from '../../utils/Form/formActions';
import { getOptions, addOption } from '../../../store/actions/productsActions';
import { connect } from 'react-redux';

const ManageOptions = (props) => {
    const [formError, setFormError] = useState(false);
    const [formSuccess, setFormSuccess] = useState(false);
    const [formdata, setFormdata] = useState({
        name: {
            element: 'input',
            value: '',
            config: {
                name: 'name_input',
                type: 'text',
                placeholder: 'Enter option'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: ''
        }
    });

    const showCategoryItems = () => (
        props.products.options ?
            props.products.options.map((item, i) => (
                <div className="category_item" key={item._id}>
                    {item.name}
                </div>
            ))
        : null
    )

    const updateForm = (element) => {
        const newFormdata = update(element, formdata, 'options');
        setFormError(false);
        setFormdata(newFormdata);
    }

    const resetFieldHandler = () => {
        const newFormdata = resetFields(formdata);
        setFormdata(newFormdata);
        setFormSuccess(true);
    }

    const submitForm = (event) => {
        event.preventDefault();

        let dataToSubmit = generateData(formdata, 'options');
        let formIsValid = isFormValid(formdata, 'options');
        let existingOptions = props.products.options;

        if(formIsValid) {
            props.dispatch(addOption(dataToSubmit, existingOptions)).then(response => {
                if(response.payload.success) {
                    resetFieldHandler();
                } else {
                    setFormError(true);
                }
            })
        } else {
            setFormError(true);
        }        
    }

    useEffect(() => {
        props.dispatch(getOptions());
    }, []);

    return (
        <div className="admin_category_wrapper">
            <h1>Options</h1>
            <div className="admin_two_column">
                <div className="left">
                    <div className="brands_container">
                        {showCategoryItems()}
                    </div>
                </div>
                <div className="right">
                    <form onSubmit={event => submitForm(event)}>
                        <FormField
                            id={'name'}
                            formdata={formdata.name}
                            change={(element) => updateForm(element)}
                        />
                        {formError ?
                            <div className="error_label">
                                Please check your data
                            </div> : null}
                        <button onClick={(event) => submitForm(event)}>Add option</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
};

export default connect(mapStateToProps)(ManageOptions);
