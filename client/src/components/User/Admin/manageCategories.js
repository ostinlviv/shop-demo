import React from 'react';
import UserLayout from '../../../hoc/user';
import ManageOptions from './manageOptions';
import ManageBrands from './manageBrands';

const ManageCategories = () => {
    return (
        <UserLayout>
            <ManageBrands/>
            <ManageOptions/>
        </UserLayout>
    )
}

export default ManageCategories;