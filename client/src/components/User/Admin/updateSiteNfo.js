import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import FormField from '../../utils/Form/formfield';

import { update, generateData, isFormValid, populateFields } from '../../utils/Form/formActions';
import { fetchSiteData, updateSiteData } from '../../../store/actions/siteActions';

export const UpdateSiteNfo = (props) => {
    const [formError, setFormError] = useState(false);
    const [formSuccess, setFormSuccess] = useState(false);
    const [formdata, setFormdata] = useState({
        address: {
            element: 'input',
            value: '',
            config: {
                label: 'Address',
                name: 'address_input',
                type: 'text',
                placeholder: 'Enter the site address'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        hours: {
            element: 'input',
            value: '',
            config: {
                label: 'Working hours',
                name: 'hours_input',
                type: 'text',
                placeholder: 'Enter the site working hours'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        phone: {
            element: 'input',
            value: '',
            config: {
                label: 'Phone',
                name: 'phone_input',
                type: 'text',
                placeholder: 'Enter phone number'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        email: {
            element: 'input',
            value: '',
            config: {
                label: 'Shop email',
                name: 'email_input',
                type: 'text',
                placeholder: 'Enter email'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        }
    });

    const updateForm = (element) => {
        const newFormdata = update(element, formdata, 'site_info');
        setFormError(false);
        setFormdata(newFormdata);
    }

    const submitForm = (event) => {
        event.preventDefault();

        let dataToSubmit = generateData(formdata, 'site_info');
        let formIsValid = isFormValid(formdata, 'site_info');

        if(formIsValid) {
            props.onUpdateSiteData(dataToSubmit).then(() => {
                setFormSuccess(true);
                setTimeout(() => {
                    setFormSuccess(false);
                }, 2000);
            });
        } else {
            setFormError(true);
        }        
    }

    useEffect(() => {
        props.onFetchSiteData();
    }, []);
    
    useEffect(() => {
        if (props.site && props.site.siteData && props.site.siteData[0]) {
            populateFields(formdata, props.site.siteData[0]);
            setFormdata({...formdata});
        }
    }, [props.site]);

    let form = "Loading";

    if (!props.site.loading) {
        form = (
            <form onSubmit={(event) => submitForm(event)}>
                <h2>Site information</h2>
                <FormField
                    id={'address'}
                    formdata={formdata.address}
                    change={(element) => updateForm(element)}
                />
                <FormField
                    id={'hours'}
                    formdata={formdata.hours}
                    change={(element) => updateForm(element)}
                />
                <FormField
                    id={'phone'}
                    formdata={formdata.phone}
                    change={(element) => updateForm(element)}
                />
                <FormField
                    id={'email'}
                    formdata={formdata.email}
                    change={(element) => updateForm(element)}
                />
                <div>
                    {
                        formSuccess ?
                            <div className="form_success">Success</div>
                        : null
                    }
                    {formError ?
                        <div className="error_label">
                            Please check your data
                            </div> : null}
                    <button onClick={(event) => submitForm(event)}>Update</button>
                </div>
            </form>
        )
    }

    return <div>{form}</div>
}

const mapStateToProps = state => {
    return {
        site: state.site
    }
}

const mapDispatchToProps = dispatch => {
    return {
      onFetchSiteData: () => dispatch(fetchSiteData()),
      onUpdateSiteData: (data) => dispatch(updateSiteData(data))
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(UpdateSiteNfo);
