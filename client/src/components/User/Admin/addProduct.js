import React, { useState, useEffect } from 'react';
import UserLayout from '../../../hoc/user';

import FormField from '../../utils/Form/formfield';
import { update, generateData, isFormValid, populateOptionFields, resetFields } from '../../utils/Form/formActions';
import FileUpload from '../../utils/Form/fileupload';

import { getBrands, getOptions, addProduct, clearProduct } from '../../../store/actions/productsActions';

import { connect } from 'react-redux';

const AddProduct = (props) => {
    const [formError, setFormError] = useState(false);
    const [formSuccess, setFormSuccess] = useState(false);
    const [formdata, setFormdata] = useState({
        name: {
            element: 'input',
            value: '',
            config: {
                label: 'Product name',
                name: 'name_input',
                type: 'text',
                placeholder: 'Enter product name'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        description: {
            element: 'textarea',
            value: '',
            config: {
                label: 'Product description',
                name: 'description_input',
                type: 'text',
                placeholder: 'Enter description'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        price: {
            element: 'input',
            value: '',
            config: {
                label: 'Product price',
                name: 'price_input',
                type: 'number',
                placeholder: 'Enter price'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        brand: {
            element: 'select',
            value: '',
            config: {
                label: 'Product brand',
                name: 'brands_input',
                options: []
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        shipping: {
            element: 'select',
            value: '',
            config: {
                label: 'Shipping',
                name: 'shipping_input',
                options: [
                    {key: true, value: 'Yes'},
                    {key: false, value: 'No'}
                ]
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        available: {
            element: 'select',
            value: '',
            config: {
                label: 'Available, in stock',
                name: 'available_input',
                options: [
                    {key: true, value: 'Yes'},
                    {key: false, value: 'No'}
                ]
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        option: {
            element: 'select',
            value: '',
            config: {
                label: 'Option One',
                name: 'option_input',
                options: []
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        publish: {
            element: 'select',
            value: '',
            config: {
                label: 'Publish',
                name: 'publish_input',
                options: [
                    {key: true, value: 'Public'},
                    {key: false, value: 'Hidden'}
                ]
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: '',
            showlabel: true
        },
        images: {
            value: [],
            validation: {
                required: false
            },
            valid: true,
            touched: false,
            validationMessage: '',
            showlabel: false
        }
    });

    useEffect(() => {
        props.dispatch(getBrands()).then(response => {
            const newFormData = populateOptionFields(formdata, response.payload.data, 'brand');
            updateFields(newFormData);
        });
        props.dispatch(getOptions()).then(response => {
            const newFormData = populateOptionFields(formdata, response.payload.data, 'option');
            updateFields(newFormData);
        });
    }, []);

    useEffect(() => {
        props.dispatch(clearProduct());
    }, [formSuccess]);

    const updateFields = (newFormData) => {
        setFormdata(newFormData)
    }

    const updateForm = (element) => {
        const newFormData = update(element, formdata, 'products');
        setFormdata(newFormData);
        setFormError(false);
    }

    const resetFieldHandler = () => {
        const newFormData = resetFields(formdata);
        setFormdata(newFormData);
        setFormSuccess(true);
        setTimeout(() => {
            setFormSuccess(false);
        }, 3000);
    }

    const submitForm = (event) => {
        event.preventDefault();

        let dataToSubmit = generateData(formdata, 'products');
        let formIsValid = isFormValid(formdata, 'products');

        if(formIsValid) {
            props.dispatch(addProduct(dataToSubmit)).then(() => {
                if(props.products.addProduct.success) {
                    resetFieldHandler();                 
                } else {
                    setFormError(true);
                }
            }).catch(e => {
                setFormError(true);
            });
        } else {
            setFormError(true);
        }        
    }

    const imagesHandler = images => {
        const newFormData = {
            ...formdata
        };
        newFormData['images'].value = images;
        newFormData['images'].valid = true;

        setFormdata(newFormData);
    }

    return (
        <UserLayout>
            <div>
                <h1>Add product</h1>
                <form onSubmit={(event) => submitForm(event)}>

                    <FileUpload
                        imagesHandler={(images) => imagesHandler(images)}
                        reset={formSuccess}
                    />

                    <FormField
                        id={'name'}
                        formdata={formdata.name}
                        change={(element) => updateForm(element)}
                    />
                    <FormField
                        id={'description'}
                        formdata={formdata.description}
                        change={(element) => updateForm(element)}
                    />
                    <FormField
                        id={'price'}
                        formdata={formdata.price}
                        change={(element) => updateForm(element)}
                    />
                    <div className="form_devider"></div>
                    <FormField
                        id={'brand'}
                        formdata={formdata.brand}
                        change={(element) => updateForm(element)}
                    />
                    <FormField
                        id={'shipping'}
                        formdata={formdata.shipping}
                        change={(element) => updateForm(element)}
                    />
                    <FormField
                        id={'available'}
                        formdata={formdata.available}
                        change={(element) => updateForm(element)}
                    />
                    <div className="form_devider"></div>
                    <FormField
                        id={'option'}
                        formdata={formdata.option}
                        change={(element) => updateForm(element)}
                    />
                    <div className="form_devider"></div>
                    <FormField
                        id={'publish'}
                        formdata={formdata.publish}
                        change={(element) => updateForm(element)}
                    />
                    {formSuccess ?
                        <div className="form_success">
                            Success
                        </div> : null}
                    {formError ?
                        <div className="error_label">
                            Please check your data
                        </div> : null}
                    <button onClick={(event) => submitForm(event)}>Add product</button>
                </form>
            </div>
        </UserLayout>        
    )    
}

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
};

export default connect(mapStateToProps)(AddProduct);
