import React, { useState, useEffect } from 'react';
import Dropzone from 'react-dropzone';
import axios from 'axios';
import UserLayout from '../../../hoc/user';
import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import faPlusCircle from '@fortawesome/fontawesome-free-solid/faPlusCircle';
import CircularProgress from '@material-ui/core/CircularProgress';

const AddFile = () => {
    const [files, setFiles] = useState([]);
    const [uploading, setUploading] = useState(false);
    const [formSuccess, setFormSuccess] = useState(false);
    const [formError, setFormError] = useState(false);
    const onDrop = files => {
        setUploading(true);
        let formData = new FormData();
        const config = {
            header: {'content-type': 'multipart/form-data'}
        }
        formData.append("file", files[0]);

        axios.post('/api/v1/users/uploadfile', formData, config)
            .then(response => {
                setUploading(false);
                setFormSuccess(true);
                setFormError(false);
            })
    }

    useEffect(() => {
        axios.get('/api/v1/users/files')
            .then(response => {
                setFiles(response.data);
            })
    }, []);

    const showFileList = () => (
        files ?
            files.map((item,i) => (
                <li key={i}>
                    <Link to={`/api/v1/users/download/${item}`} target="_blank">
                        {item}
                    </Link>
                </li>
            ))
        : null
    )
    
    return (
        <UserLayout>
            <h1>Upload file</h1>
            <Dropzone
                onDrop={(e) => onDrop(e)}
                multiple={false}                            
            >
                {({getRootProps, getInputProps}) => (
                    <div className="dropzone_box">
                        <div className="wrap" {...getRootProps()}>
                            <input {...getInputProps()} />
                            <FontAwesomeIcon
                                icon={faPlusCircle}
                            />
                        </div>
                    </div>
                )}
            </Dropzone>
            {
                uploading ?
                    <div className="dropzone_box" style={{
                        textAlign: "center",
                        paddingTop: '60px'
                    }}>
                        <CircularProgress
                            style={{ color: '#00bcd4' }}
                            thickness={7}
                        />
                    </div>
                    : null
            }
            <div style={{clear: 'both'}}>
                {formSuccess ?
                    <div className="form_success">
                        Success
                        </div> : null}
                {formError ?
                    <div className="error_label">
                        Please check your data
                        </div> : null}
            </div>
            <hr/>
            <div>
                <ul>
                    {showFileList()}
                </ul>
            </div>
        </UserLayout>
    )
}

export default AddFile;