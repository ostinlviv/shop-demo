import React from 'react';
import UserLayout from '../../hoc/user';
import UpdatePersonalNfo from './updatePersonalNfo';

const UpdateProfile = () => {
    return (
        <UserLayout>
            <h1>Profile</h1>
            <UpdatePersonalNfo/>
        </UserLayout>
    )
}

export default UpdateProfile;