import React, { useState, useEffect } from 'react';
import UserLayout from '../../hoc/user';

import { connect } from 'react-redux';
import { getCartItems, removeCartItem, onSuccessBuy } from '../../store/actions/userActions';
import UserProductBlock from '../utils/User/productBlock';
import Paypal from '../utils/paypal';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import faFrown from '@fortawesome/fontawesome-free-solid/faFrown';
import faSmile from '@fortawesome/fontawesome-free-solid/faSmile';

const UserCart = (props) => {
    const [total, setTotal] = useState(0);
    const [showTotal, setShowTotal] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);

    useEffect(() => {
        let cartItem = [];
        let user = props.user;
        if(user.userData.cart) {
            if(user.userData.cart.length > 0) {
                user.userData.cart.forEach(item => {
                    cartItem.push(item.id);
                });
                props.dispatch(getCartItems(cartItem, user.userData.cart))
                    .then((response) => {
                        if(response.payload.length > 0) {
                            calculateTotal(response.payload);
                        }
                    });
            }
        }
    }, []);

    const removeFromCart = id => {
        props.dispatch(removeCartItem(id))
            .then((response) => {
                if(response.payload.cartDetail.length <= 0) {
                    setShowTotal(false);
                } else {
                    calculateTotal(response.payload.cartDetail)
                }
            })
    }

    const calculateTotal = (cartDetail) => {
        let calcTotal = 0;

        cartDetail.forEach(item => {
            calcTotal += parseInt(item.price, 10) * item.quantity;
        });

        setTotal(calcTotal);
        setShowTotal(true);
    }

    const showNoItemMessage = () => (
        <div className="cart_no_items">
            <FontAwesomeIcon icon={faFrown} />
            <div>You have no items</div>
        </div>
    )

    const transactionError = () => {
        console.log('Paypal error');
    }

    const transactionCancelled = () => {
        console.log('Transaction cancelled');
    }

    const transactionSuccess = (data) => {
        props.dispatch(onSuccessBuy({
            cartDetail: props.user.cartDetail,
            paymentData: data
        })).then((response) => {
            if(response.payload.success){
                setShowTotal(false);
                setShowSuccess(true);
            }
        })        
    }

    return (
        <UserLayout>
            <div>
                <h1>My Cart</h1>
                <div className="user_cart">
                    <UserProductBlock
                        products={props.user}
                        type="cart"
                        removeItem={id => removeFromCart(id)}
                    />
                    {
                        showTotal ?
                            <div className="user_cart_sum">
                                <div>
                                    Total amount: $ {total}
                                </div>
                            </div>
                        : showSuccess ?
                            <div className="cart_success">
                                <FontAwesomeIcon icon={faSmile} />
                                <div>THANK YOU</div>
                                <div>YOUR ORDER IS NOW COMPLETE</div>
                            </div>                           
                            : showNoItemMessage()
                    }
                </div>
                {
                    showTotal ?
                        <div className="paypal_button_container">
                            <Paypal
                                toPay={total}
                                transactionError={data => transactionError(data)}
                                transactionCanceled={data => transactionCancelled(data)}
                                onSuccess={data => transactionSuccess(data)}
                            />
                        </div>
                    : null
                }
            </div>
        </UserLayout>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps)(UserCart);