import React, { useState, useEffect } from 'react';
import FormField from '../utils/Form/formfield';

import { connect } from 'react-redux';

import { update, generateData, isFormValid, populateFields } from '../utils/Form/formActions';
import { updateUserData, clearUpdateUser } from '../../store/actions/userActions';

const UpdatePersonalNfo = (props) => {
    const [formError, setFormError] = useState(false);
    const [formSuccess, setFormSuccess] = useState(false);
    const [formdata, setFormdata] = useState({
        name: {
            element: 'input',
            value: '',
            config: {
                name: 'name_input',
                type: 'text',
                placeholder: 'Enter your name'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: ''
        },
        lastname: {
            element: 'input',
            value: '',
            config: {
                name: 'lastname_input',
                type: 'text',
                placeholder: 'Enter your lastname'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: ''
        },
        email: {
            element: 'input',
            value: '',
            config: {
                name: 'email_input',
                type: 'email',
                placeholder: 'Enter your email'
            },
            validation: {
                required: true,
                email: true
            },
            valid: false,
            touched: false,
            validationMessage: ''
        }
    });

    const updateForm = (element) => {
        const newFormdata = update(element, formdata, 'update_user');
        setFormError(false);
        setFormdata(newFormdata);
    }

    const submitForm = (event) => {
        event.preventDefault();

        let dataToSubmit = generateData(formdata, 'register');
        let formIsValid = isFormValid(formdata, 'register');

        if(formIsValid) {
           props.dispatch(updateUserData(dataToSubmit)).then(() => {
                setFormSuccess(true);
                setTimeout(() => {
                    props.dispatch(clearUpdateUser());
                    setFormSuccess(false);
                }, 2000);
            })
        } else {
            setFormError(true);
        }        
    }
    useEffect(() => {
        populateFields(formdata, props.user.userData);
        setFormdata({...formdata});
    }, []);

    return (
        <div>
            <form onSubmit={(event) => submitForm(event)}>
                <h2>Personal information</h2>
                <div className="form_block_two">
                    <div className="block">
                        <FormField
                            id={'name'}
                            formdata={formdata.name}
                            change={(element) => updateForm(element)}
                        />
                    </div>
                    <div className="block">
                        <FormField
                            id={'lastname'}
                            formdata={formdata.lastname}
                            change={(element) => updateForm(element)}
                        />
                    </div>
                </div>
                <div>
                    <FormField
                        id={'email'}
                        formdata={formdata.email}
                        change={(element) => updateForm(element)}
                    />
                </div>
                <div>
                    {
                        formSuccess ?
                            <div className="form_success">Success</div>
                        : null
                    }
                    {formError ?
                        <div className="error_label">
                            Please check your data
                            </div> : null}
                    <button onClick={(event) => submitForm(event)}>Update personal info</button>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps)(UpdatePersonalNfo);