import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { getProductsToShop, getBrands, getOptions } from '../../store/actions/productsActions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import faBars from '@fortawesome/fontawesome-free-solid/faBars';
import faTh from '@fortawesome/fontawesome-free-solid/faTh';

import PageTop from '../utils/pageTop';
import CollapseCheckbox from '../utils/collapseCheckbox';
import CollapseRadio from '../utils/collapseRadio';
import LoadmoreCards from './loadmoreCards';
import { price } from '../utils/Form/fixedCategories';

const Shop = (props) => {
    const [grid, setGrid] = useState('');
    const [limit, setLimit] = useState(6);
    const [skip, setSkip] = useState(0);
    const [filters, setFilters] = useState({
        brand: [],
        option: [],
        price: []
    });

    useEffect(() => {
        props.dispatch(getBrands());
        props.dispatch(getOptions());
        props.dispatch(getProductsToShop(
            skip,
            limit,
            filters
        ));
    }, []);

    const handlePrice = (value) => {
        const data = price;
        let array = [];

        for(let key in data) {
            if(data[key]._id === parseInt(value, 10)) {
                array = data[key].array;
            }
        }
        return array;
    }

    const handleFilters = (checkedFilters, category) => {
        const newFilters = {...filters}
        newFilters[category] = checkedFilters;
        if(category === 'price') {
            let priceValues = handlePrice(checkedFilters);
            newFilters[category] = priceValues;
        }
        showFilteredResults(newFilters);
        setFilters(newFilters);
    }

    const showFilteredResults = showFilters => {
        props.dispatch(getProductsToShop(
            0,
            limit,
            showFilters
        )).then(() => {
            setSkip(0);
        });
    }

    const loadMoreCards = () => {
        let showSkip = skip + limit;

        props.dispatch(getProductsToShop(
            showSkip,
            limit,
            filters,
            props.products.toShop
        )).then(() => {
            setSkip(showSkip)
        });
    }

    const handleGrid = () => {
        setGrid(!grid ? 'grid_bars' : '');
    }

    const products = props.products;
    
    return (
        <div>
            <PageTop title="Browse Products" />
            <div className="container">
                <div className="shop_wrapper">
                    <div className="left">
                        <CollapseCheckbox
                            initState={true}
                            title="Brands"
                            list={products.brands}
                            handleFilters={(checkedFilters) => handleFilters(checkedFilters, 'brand')}
                        />
                        <CollapseCheckbox
                            initState={false}
                            title="Option"
                            list={products.options}
                            handleFilters={(checkedFilters) => handleFilters(checkedFilters, 'option')}
                        />
                        <CollapseRadio
                            initState={true}
                            title="Price"
                            list={price}
                            handleFilters={(checkedFilters) => handleFilters(checkedFilters, 'price')}
                        />
                    </div>
                    <div className="right">
                        <div className="shop_options">
                            <div className="shop_grids clear">
                                <div
                                    className={`grid_btn ${grid ? '' : 'active'}`}
                                    onClick={() => handleGrid()}
                                >
                                    <FontAwesomeIcon icon={faTh} />
                                </div>
                                <div
                                    className={`grid_btn ${grid ? '' : 'active'}`}
                                    onClick={() => handleGrid()}
                                >
                                    <FontAwesomeIcon icon={faBars} />
                                </div>
                            </div>                                
                        </div>
                        <div>
                            <LoadmoreCards
                                grid={grid}
                                limit={limit}
                                size={products.toShopSize}
                                products={products.toShop}
                                loadMore={() => loadMoreCards()}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
};

export default connect(mapStateToProps)(Shop);
