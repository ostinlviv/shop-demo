import React, { useState, useEffect } from 'react';
import axios from 'axios';
import FormField from '../utils/Form/formfield';
import { update, generateData, isFormValid } from '../utils/Form/formActions';
import { Dialog } from '@material-ui/core';

const ResetPass = (props) => {
    const [formError, setFormError] = useState(false);
    const [formErrorMessage, setFormErrorMessage] = useState('');
    const [resetToken, setResetToken] = useState('');
    const [formSuccess, setFormSuccess] = useState(false);
    const [formdata, setFormdata] = useState({
        password: {
            element: 'input',
            value: '',
            config: {
                name: 'password_input',
                type: 'password',
                placeholder: 'Enter your password'
            },
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            validationMessage: ''
        },
        confirmPassword: {
            element: 'input',
            value: '',
            config: {
                name: 'confirm_password_input',
                type: 'password',
                placeholder: 'Confirm your password'
            },
            validation: {
                required: true,
                confirm: 'password'
            },
            valid: false,
            touched: false,
            validationMessage: ''
        },
    });

    const updateForm = (element) => {
        const newFormdata = update(element, formdata, 'reset_pass');
        setFormError(false);
        setFormdata(newFormdata);
    }

    const submitForm = (event) => {
        event.preventDefault();

        let dataToSubmit = generateData(formdata, 'reset_pass');
        let formIsValid = isFormValid(formdata, 'reset_pass');

        if(formIsValid) {
           axios.post(`/api/v1/auth/resetpassword`, {
            ...dataToSubmit,
            token: resetToken
        }).then(response => {
               if(!response.data.success) {
                    setFormError(true);
                    setFormErrorMessage(response.data.message);
               } else {
                    setFormError(false);
                    setFormSuccess(true);
                    setTimeout(() => {
                        props.history.push('/login');
                    }, 3000);
               }
           })
        } else {
            setFormError(true);
        }        
    }

    useEffect(() => {
        const resetToken = props.match.params.token;
        setResetToken({ resetToken })
    }, []);


    return (
        <div className="container">
            <form onSubmit={(event) => submitForm(event)}>
                <h2>Reset password</h2>
                <div className="form_block_two">
                    <div className="block">
                        <FormField
                            id={'password'}
                            formdata={formdata.password}
                            change={(element) => updateForm(element)}
                        />
                    </div>
                    <div className="block">
                        <FormField
                            id={'confirmPassword'}
                            formdata={formdata.confirmPassword}
                            change={(element) => updateForm(element)}
                        />
                    </div>
                </div>
                <div>
                    { formError ? 
                        <div className="error_label">
                            {formErrorMessage}
                        </div> : null }
                    <button onClick={(event) => submitForm(event)}>Reset password</button>
                </div>
            </form>
            <Dialog open={formSuccess}>
                <div className="dialog_alert">
                    <div>Alright!</div>
                    <div>Your password was resetted... You will be redirected to login page</div>
                </div>
            </Dialog>
        </div>
    )
}

export default ResetPass;