import React, { useState } from 'react';
import axios from 'axios';

import FormField from '../utils/Form/formfield';
import { update, generateData, isFormValid } from '../utils/Form/formActions';

const ResetUser = (props) => {
    const [formError, setFormError] = useState(false);
    const [formSuccess, setFormSuccess] = useState(false);
    const [formdata, setFormdata] = useState({
        email: {
            element: 'input',
            value: '',
            config: {
                name: 'email_input',
                type: 'email',
                placeholder: 'Enter your email'
            },
            validation: {
                required: true,
                email: true
            },
            valid: false,
            touched: false,
            validationMessage: ''
        }
    });

    const updateForm = (element) => {
        const newFormdata = update(element, formdata, 'reset_email');
        setFormError(false);
        setFormdata(newFormdata);
    }

    const submitForm = (event) => {
        event.preventDefault();

        let dataToSubmit = generateData(formdata, 'reset_email');
        let formIsValid = isFormValid(formdata, 'reset_email');

        if(formIsValid) {
           axios.post('/api/v1/auth/forgotpassword', dataToSubmit)
                .then(response => {
                    if(response.data.success) {
                        setFormSuccess(true);
                    }
                })
        } else {
            setFormError(true);
        }        
    }

    return (
        <div className="container">
            <h1>Reset password</h1>
            <form onSubmit={(event) => submitForm(event)}>
                <FormField
                    id={'email'}
                    formdata={formdata.email}
                    change={(element) => updateForm(element)}
                />
                {
                    formSuccess ?
                        <div className="form_success">Done, check your email</div>
                    : null
                }
                { formError ? 
                    <div className="error_label">
                        Please check your data
                    </div> : null }
                <button onClick={(event) => submitForm(event)}>Send email to reset password</button>
            </form>
        </div>
    )
}

export default ResetUser;