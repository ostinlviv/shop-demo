import React, { useState, useEffect } from 'react';
import ImageLightBox from '../utils/lightbox';

const ProdImg = (props) => {
    const [lightbox, setLightbox] = useState(false);
    const [imagePos, setImagePos] = useState(0);
    const [lightboxImages, setLightboxImages] = useState([]);

    useEffect(() => {
        if(props.detail.images.length > 0) {
            let images = [];
            props.detail.images.forEach(item => {
                images.push(item.url);
            });
            setLightboxImages(images);
        }
    }, []);


    const renderCardImage = images => {
        if(images.length > 0) {
            return images[0].url
        } else {
            return "/images/image_not_available.png"
        }
    }
    const showThumbs = (data) => (
        data.images.map((item, i) => (
            i > 0 ?
                <div
                    key={i}
                    onClick={() => handleLightbox(i)}
                    className="thumb"
                    style={{background: `url(${item.url}) no-repeat`}}
                ></div>
            : null
        ))
    );

    const handleLightbox = (pos) => {
        if(lightboxImages.length > 0) {
            setLightbox(true);
            setImagePos(pos);
        }
    }
    const handleLightBoxClose = () => {
        setLightbox(false);
    }
    
    const { detail } = props;
    return (
        <div className="product_image_container">
            <div className="main_pic">
                <div 
                    style={{ background: `url(${renderCardImage(detail.images)}) no-repeat` }}
                    onClick={() => handleLightbox(0)}
                ></div>
            </div>
            <div className="main_thumbs">
                {showThumbs(detail)}
            </div>
            {lightbox ? (
                <ImageLightBox
                    id={detail._id}
                    images={lightboxImages}
                    pos={imagePos}
                    isOpen={lightbox}
                    onClose={() => handleLightBoxClose()}
                />
            ) : null}
                
            </div>
    )
}

export default ProdImg;