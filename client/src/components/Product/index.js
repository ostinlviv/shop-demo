import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { addToCart } from '../../store/actions/userActions'
import { clearProductDetail, getProductDetail } from '../../store/actions/productsActions';
import PageTop from '../utils/pageTop';
import ProdNfo from './prodNfo';
import ProdImg from './prodImg';

const ProductPage = (props) => {

    useEffect(() => {        
        const id = props.match.params.id;
        props.dispatch(getProductDetail(id)).then(response => {            
            if(!response.payload) {
                props.history.push('/');
            }
        });
        return () => {
            props.dispatch(clearProductDetail());
        }
    }, []);

    const addToCartHandler = (id) => {
        props.dispatch(addToCart(id));
    }


    return (
        <div>
            <PageTop
                title="Product detail"
            />
            <div className="container">
                {
                    props.products.prodDetail ?
                        <div className="product_detail_wrapper">
                            <div className="left">
                                <div style={{width: '500px'}}>
                                    <ProdImg
                                        detail={props.products.prodDetail}
                                    />
                                </div>
                            </div>
                            <div className="right">
                                <ProdNfo
                                    addToCart={(id) => addToCartHandler(id)}
                                    detail={props.products.prodDetail}
                                />
                            </div>
                        </div>
                    : 'Loading...'
                }
            </div>
        </div>
    )

}

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
};

export default connect(mapStateToProps)(ProductPage);
