const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
// const fileUpload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const cors = require('cors');
const path = require('path');
const connectDB = require('./config/db');
const errorHandler = require('./middleware/error');

dotenv.config({ path: path.resolve(__dirname, './config/config.env') });
connectDB();

const products = require('./routes/products');
const comments = require('./routes/comments');
const auth = require('./routes/auth');
const users = require('./routes/users');
const site = require('./routes/site');

const app = express();

app.use(express.json());

app.use(cookieParser());

if(process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// app.use(fileUpload());

app.use(mongoSanitize());
app.use(helmet());
app.use(xss());
const limiter = rateLimit({
    windowsMs: 10 * 60 * 1000,
    max: 100
});
app.use(limiter);
app.use(hpp());
app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/v1/products', products);
app.use('/api/v1/comments', comments);
app.use('/api/v1/auth', auth);
app.use('/api/v1/users', users);
app.use('/api/v1/site', site);

app.use(errorHandler);

if(process.env.NODE_ENV === 'production') {
    const root = require('path').join(__dirname, '../client', 'build')
    app.use(express.static(root));
    app.get("*", (req, res) => {
        res.sendFile('index.html', { root });
    })
}

const PORT = process.env.PORT || 5000;

const server = app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold));

process.on('unhandledRejection', (err, promise) => {
    console.log(`Error: ${err.message}`.red);
    server.close(() => process.exit(1));
});