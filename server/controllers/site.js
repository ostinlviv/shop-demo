const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Site = require('../models/Site');

// @desc    Get site data
// @route   GET /api/v1/site
// @access  Public
exports.getSiteData = asyncHandler(async (req, res, next) => {
    const site = await Site.find();
    res.status(200).json({
        success: true,
        data: site.length > 0 ? site[0].siteInfo : {}
    });
});

// @desc    Update site data
// @route   POST /api/v1/site
// @access  Private
exports.updateSiteData = asyncHandler(async (req, res, next) => {
    const isSite = await Site.findOne({ name: 'Site' });
    let site;
    if (isSite) {
        site = await Site.findOneAndUpdate(
            { name: 'Site' },
            { "$set": { siteInfo: req.body } },
            { new: true }
        );
    } else {
        site = await Site.create(
            { name: 'Site', siteInfo: req.body }
        );
    }
    
    res.status(200).json({
        success: true,
        data: site.siteInfo
    });
});