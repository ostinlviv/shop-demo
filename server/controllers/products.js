
const path = require('path');
const mongoose = require('mongoose');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Product = require('../models/Product');
const Option = require('../models/Option');
const Brand = require('../models/Brand');

// @desc    Get all products
// @route   GET /api/v1/products
// @access  Public
exports.getProducts = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
});

// @desc    Get single product
// @route   GET /api/v1/products/product?id=asdasd&type=single
// @access  Public
exports.getProduct = asyncHandler(async (req, res, next) => {
    let type = req.query.type;
    let items = req.query.id;

    if(type === "array") {
        let ids = req.query.id.split(',');
        items = [];
        items = ids.map(item => {
            return mongoose.Types.ObjectId(item);
        });
    }
    const product = await Product
        .find({ '_id': { $in: items } })
        .populate('brand')
        .populate('option');

    if(!product) {
        return next(
            new ErrorResponse(`Resource not found with id of ${req.params.id}`, 404)
        );
    }

    res.status(200).json({ success: true, data: product });
});

// @desc    Create new product
// @route   POST /api/v1/products
// @access  Private
exports.createProduct = asyncHandler(async (req, res, next) => {    
    const product = await Product.create(req.body);

    res.status(201).json({ success: true, data: product });
});

// @desc    Get products for shop
// @route   POST /api/v1/products/shop
// @access  Private
exports.shopProducts = asyncHandler(async (req, res, next) => {
    let order = req.body.order ? req.body.order : "desc";
    let sortBy = req.body.sortBy ? req.body.sortBy : "_id";
    let limit = req.body.limit ? parseInt(req.body.limit) : 100;
    let skip = parseInt(req.body.skip);
    let findArgs = {};

    for(let key in req.body.filters) {
        if(req.body.filters[key].length > 0) {
            if(key === 'price') {
                findArgs[key] = {
                    $gte: req.body.filters[key][0],
                    $lte: req.body.filters[key][1],
                }
            } else {
                findArgs[key] = req.body.filters[key];
            }
        }
    }
    findArgs['publish'] = true;

    const products = await Product
        .find(findArgs)
        .populate('brand')
        .populate('option')
        .sort([[sortBy, order]])
        .skip(skip)
        .limit(limit);

    res.status(200).json({
        success: true,
        size: products.length,
        products
    });
});

// @desc    Update product
// @route   PUT /api/v1/products/:id
// @access  Private
exports.updateProduct = asyncHandler(async (req, res, next) => {
    let product = await Product.findById(req.params.id);

    if(!product) {
        return next(
            new ErrorResponse(`Resource not found with id of ${req.params.id}`, 404)
        );
    }

    if(product.user.toString() !== req.user.id & req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User ${req.params.id} is not authorized to update this post`, 401)
        );
    }

    product = await Product.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({ success: true, data: product });
});

// @desc    Delete product
// @route   DELETE /api/v1/products/:id
// @access  Private
exports.deleteProduct = asyncHandler(async (req, res, next) => {
    const product = await Post.findById(req.params.id);
    if(!product) {
        return next(
            new ErrorResponse(`Resource not found with id of ${req.params.id}`, 404)
        );
    }

    if(product.user.toString() !== req.user.id & req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User ${req.params.id} is not authorized to delete this post`, 401)
        );
    }

    product.remove();

    res.status(200).json({ success: true, data: {} });
});

// @desc    Upload photo for product
// @route   PUT /api/v1/products/:id/photo
// @access  Private
exports.productPhotoUpload = asyncHandler(async (req, res, next) => {
    const product = await Product.findById(req.params.id);
    if(!product) {
        return next(
            new ErrorResponse(`Resource not found with id of ${req.params.id}`, 404)
        );
    }

    if(product.user.toString() !== req.user.id & req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User ${req.params.id} is not authorized to update this product`, 401)
        );
    }

    if(!req.files) {
        return next(new ErrorResponse(`Please upload a file`, 400));
    }

    const file = req.files.file;
    if(!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse(`Please upload an image file`, 400));
    }
    if(file.size > process.env.MAX_FILE_UPLOAD) {
        return next(new ErrorResponse(`Please upload an image less than ${process.env.MAX_FILE_UPLOAD} bytes`, 400));
    }

    file.name = `photo_${product.id}${path.parse(file.name).ext}`;

    file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if (err) {
            console.error(err);
            return next(new ErrorResponse(`Problem with file upload`, 500));
        }

        await Product.findByIdAndUpdate(req.params.id, { photo: file.name });

        res.status(200).json({
            success: true,
            data: file.name
        });
    });   
});

// @desc    Get options
// @route   GET /api/v1/products/options
// @access  Public
exports.getOptions = asyncHandler(async (req, res, next) => {
    const options = await Option.find({});   
    res.status(200).json({ success: true, data: options });
});

// @desc    Add option
// @route   POST /api/v1/products/options
// @access  Public
exports.addOption = asyncHandler(async (req, res, next) => {
    const option = await Option.create(req.body);
    res.status(200).json({ success: true, data: option });
});

// @desc    Get brands
// @route   GET /api/v1/products/brands
// @access  Public
exports.getBrands = asyncHandler(async (req, res, next) => {
    const brands = await Brand.find({});   

    res.status(200).json({ success: true, data: brands });
});

// @desc    Add brand
// @route   POST /api/v1/products/brands
// @access  Public
exports.addBrand = asyncHandler(async (req, res, next) => {
    const brand = await Brand.create(req.body);
    res.status(200).json({ success: true, data: brand });
});