const cloudinary = require('cloudinary');
const path = require('path');
var fs = require('fs');
const async = require('async');
const sha1 = require('crypto-js/sha1');
const mongoose = require('mongoose');
require('dotenv').config({ path: path.resolve(__dirname, './config/config.env') });
cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUD_API_KEY,
    api_secret: process.env.CLOUD_API_SECRET,
});

const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const User = require('../models/User');
const Payment = require('../models/Payment');
const Product = require('../models/Product');

const multer = require('multer');
let storage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null, path.resolve('.')+'/server/public/uploads/')
    },
    filename: (req,file,cb) => {
        cb(null, `${Date.now()}_${file.originalname}`)
    }
});

const upload = multer({storage: storage}).single('file');


// @desc    Get users
// @route   GET /api/v1/users
// @access  Private/admin
exports.getUsers = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
});

// @desc    Get single user
// @route   GET /api/v1/users/:id
// @access  Private/admin
exports.getUser = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.params.id);

    res.status(200).json({
        success: true,
        data: user
    });
});

// @desc    Create user
// @route   POST /api/v1/users/
// @access  Private/admin
exports.createUser = asyncHandler(async (req, res, next) => {
    const user = await User.create(req.body);

    res.status(201).json({
        success: true,
        data: user
    });
});

// @desc    Update user
// @route   PUT /api/v1/users
// @access  Private
exports.updateUser = asyncHandler(async (req, res, next) => {
    const user = await User.findByIdAndUpdate(req.user._id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: user
    });
});

// @desc    Delete user
// @route   DELETE /api/v1/users/:id
// @access  Private/admin
exports.deleteUser = asyncHandler(async (req, res, next) => {
    await User.findByIdAndDelete(req.params.id);

    res.status(200).json({
        success: true,
        data: {}
    });
});

// @desc    Upload image
// @route   POST /api/v1/users/uploadimage
// @access  Private/admin
exports.uploadImage = asyncHandler(async (req, res, next) => {
    cloudinary.uploader.upload(req.files.file.path, (result) => {
        res.status(200).send({
            public_id: result.public_id,
            url: result.url
        });
    }, {
        public_id: `${Date.now()}`,
        resource_type: 'auto'
    });
});


// @desc    Remove image
// @route   GET /api/v1/users/removeimage
// @access  Private/admin
exports.removeImage = asyncHandler(async (req, res, next) => {
    let image_id = req.query.public_id;
    cloudinary.uploader.destroy(image_id, (error, result) => {
        if(error) return res.json({success: false, error});
        res.status(200).json({
            success: true,
        });
    });
});

// @desc    Upload file
// @route   POST /api/v1/users/uploadfile
// @access  Private/admin
exports.uploadFile = asyncHandler(async (req, res, next) => {
    upload(req,res,(err) => {
        if(err) {
            return res.json({success: false, err})
        }
        return res.json({success: true})
    })
});

// @desc    Get files
// @route   GET /api/v1/users/files
// @access  Private/admin
exports.getFiles = asyncHandler(async (req, res, next) => {
    const dir = path.resolve('.')+'/server/public/uploads/';
    fs.readdir(dir, (err, items) => {
        return res.status(200).send(items);
    })
});

// @desc    Download file by id
// @route   GET /api/v1/users/download/:id
// @access  Private/admin
exports.downloadFile = asyncHandler(async (req, res, next) => {
    const file = path.resolve('.')+`/server/public/uploads/${req.params.id}`;
    res.download(file);
});

// @desc    Add to cart
// @route   GET /api/v1/users/add-to-cart
// @access  Private
exports.addToCart = asyncHandler(async (req, res, next) => {
    let user = await User.findOne({_id: req.user._id});

    let duplicate = false;

    user.cart.forEach((item) => {
        if(item.id == req.query.productId) {
            duplicate = true
        }
    });
    
    if(duplicate) {
        user = await User.findOneAndUpdate(
            {_id: req.user._id, "cart.id": mongoose.Types.ObjectId(req.query.productId)},
            {$inc: {"cart.$.quantity": 1}},
            {new: true});
    } else {
        user = await User.findOneAndUpdate(
            {_id: req.user._id},
            {$push: {
                cart: {
                    id: mongoose.Types.ObjectId(req.query.productId),
                    quantity: 1,
                    date: Date.now()
                }
            }},
            {new: true});
    }
    
    res.status(200).json(user.cart);
});

// @desc    Remove from cart
// @route   GET /api/v1/users/remove-from-cart
// @access  Private
exports.removeFromCart = asyncHandler(async (req, res, next) => {
    const user = await User.findOneAndUpdate(
        {_id: req.user._id},
        {"$pull": {
            "cart": {
                "id": mongoose.Types.ObjectId(req.query._id)
            }
        }},
        {new: true});
    
    let cart = user.cart;
    let array = cart.map(item => {
        return mongoose.Types.ObjectId(item.id);
    });
    const product = await Product.find({'_id': {$in: array}})
        .populate('brand')
        .populate('wood');

    return res.status(200).json({
        cartDetail: product,
        cart
    });
});

// @desc    Success buy
// @route   POST /api/v1/users/success-buy
// @access  Private
exports.successBuy = asyncHandler(async (req, res, next) => {
    let history = [];
    let transactionData = {};

    const date = new Date();
    const po = `PO-${date.getSeconds()}${date.getMilliseconds()}-${sha1(req.user._id).toString().substring(0,8)}`

    req.body.cartDetail.forEach((item) => {
        history.push({
            porder: po,
            dateOfPurchase: Date.now(),
            name: item.name,
            brand: item.brand.name,
            id: item._id,
            price: item.price,
            quantity: item.quantity,
            paymentId: req.body.paymentData.paymentID
        });
    });

    transactionData.user = {
        id: req.user._id,
        name: req.user.name,
        lastname: req.user.lastname,
        email: req.user.email
    }
    transactionData.data = {
        ...req.body.paymentData,
        porder: po
    };
    transactionData.product = history;

    const user = await User.findOneAndUpdate(
        { _id: req.user._id },
        { $push: { history: history }, $set: { cart: [] } },
        { new: true });

    const payment = await Payment.create(transactionData);
    let products = [];
    payment.product.forEach(item => {
        products.push({id: item.id, quantity: item.quantity})
    });
    
    async.eachSeries(products, (item, callback) => {
        Product.update(
            {_id: item.id },
            { $inc: {
                "sold": item.quantity
            }},
            {new: false},
            callback
        )
    }, (err) => {
        if(err) return res.json({success: false, err});
        // sendEmail(user.email, user.name, null, "purchase", transactionData);
        res.status(200).json({
            success: true,
            cart: user.cart,
            cartDetail: []
        });
    });    
});