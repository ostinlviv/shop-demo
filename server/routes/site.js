const express = require('express');
const {
    getSiteData,
    updateSiteData
} = require('../controllers/site');
const router = express.Router();
const { protect, authorize } = require('../middleware/auth');


router.route('/')
    .get(getSiteData)
    .post(protect, authorize('admin'), updateSiteData);

module.exports = router;