const express = require('express');
const formidable = require('express-formidable');
const {
    getUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser,
    uploadImage,
    removeImage,
    uploadFile,
    getFiles,
    downloadFile,
    addToCart,
    removeFromCart,
    successBuy
} = require('../controllers/users');

const User = require('../models/User');

const router = express.Router({ mergeParams: true });

const advancedResults = require('../middleware/advancedResults');
const { protect, authorize } = require('../middleware/auth');


router.route('/')
    .get(protect, authorize('admin'), advancedResults(User), getUsers)
    .post(protect, authorize('admin'), createUser)
    .put(protect, updateUser);

router.route('/add-to-cart')
    .post(protect, addToCart);

router.route('/remove-from-cart')
    .get(protect, removeFromCart);

router.route('/success-buy')
    .post(protect, successBuy);

router.route('/uploadimage')
    .post(protect, authorize('admin'), formidable(), uploadImage)

router.route('/removeimage')
    .get(protect, authorize('admin'), removeImage)

router.route('/uploadfile')
    .post(protect, authorize('admin'), uploadFile)

router.route('/files')
    .get(protect, authorize('admin'), getFiles)

router.route('/download/:id')
    .get(protect, authorize('admin'), downloadFile)

router.route('/:id')
    .get(protect, getUser)
    .delete(protect, authorize('admin'), deleteUser);

module.exports = router;