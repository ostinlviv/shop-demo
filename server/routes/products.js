const express = require('express');
const {
    getProducts,
    getProduct,
    createProduct,
    updateProduct,
    deleteProduct,
    productPhotoUpload,
    shopProducts,
    getOptions,
    getBrands,
    addOption,
    addBrand
} = require('../controllers/products');

const Product = require('../models/Product');

const commentRouter = require('./comments');

const router = express.Router();

const advancedResults = require('../middleware/advancedResults');
const { protect, authorize } = require('../middleware/auth');

router.use('/:productId/comments', commentRouter);

router
    .route('/')
    .get(advancedResults(Product, 'comments brand option'), getProducts)
    .post(protect, authorize('publisher', 'admin'), createProduct);

router
    .route('/shop')
    .post(shopProducts)

router
    .route('/options')
    .get(getOptions)
    .post(addOption)

router
    .route('/brands')
    .get(getBrands)
    .post(addBrand)

router
    .route('/product')
    .get(getProduct)

router
    .route('/:id')
    .put(protect, authorize('publisher', 'admin'), updateProduct)
    .delete(protect, authorize('publisher', 'admin'), deleteProduct);

router
    .route('/:id/photo')
    .put(protect, authorize('publisher', 'admin'), productPhotoUpload);

module.exports = router;