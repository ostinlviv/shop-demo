const nodemailer = require('nodemailer');
require('dotenv').config();
const { welcome } = require('./mailTemplates/welcome_template');
const { purchase } = require('./mailTemplates/purchase_template');
const { resetPass } = require('./mailTemplates/resetpass_template');

const getEmailData = (to, name, token, template, actionData) => {
    let data = null;
    switch(template) {
        case "welcome":
            data = {
                from: `${process.env.FROM_NAME} <${process.env.FROM_EMAIL}>`,
                to,
                subject: `Welcome to Waves ${name}`,
                html: welcome()
            }
            break;
        case "purchase":
            data = {
                from: `${process.env.FROM_NAME} <${process.env.FROM_EMAIL}>`,
                to,
                subject: `Thank you for buying ${name}`,
                html: purchase(actionData)
            }
            break;
        case "reset_password":
            data = {
                from: `${process.env.FROM_NAME} <${process.env.FROM_EMAIL}>`,
                to,
                subject: `Hey ${name}, reset your password`,
                html: resetPass(actionData)
            }
            break;
        default:
            data;
    }
    return data;
}

const sendEmail = async (to, name, token, type, actionData = null) => {
    const transporter = nodemailer.createTransport({
        service: "Gmail",
        auth: {
            user: process.env.SMTP_EMAIL,
            pass: process.env.SMTP_PASSWORD
        }
    });

    const mail = getEmailData(to, name, token, type, actionData);

    const info = await transporter.sendMail(mail);

    console.log('Message sent: %s', info.messageId);
}

module.exports = sendEmail;