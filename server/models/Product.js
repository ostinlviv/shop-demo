const mongoose = require('mongoose');
const slugify = require('slugify');

const ProductSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      maxlength: 100,
      unique: 1
    },
    description: {
      type: String,
      required: true,
      maxlength: 100000
    },
    price: {
      type: Number,
      required: true,
      maxlength: 255
    },
    brand: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Brand',
      required: true
    },
    shipping: {
      type: Boolean,
      required: true
    },
    available: {
      required: true,
      type: Boolean
    },
    slug: String,
    option: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Option',
      required: true
    },
    sold: {
      type: Number,
      default: 0,
      maxlength: 100
    },
    publish: {
      required: true,
      type: Boolean
    },
    images: {
      type: Array,
      default: []
    },
    createdAt: {
      type: Date,
      default: Date.now
    }
  }
);

ProductSchema.pre('save', function(next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

ProductSchema.pre('remove', async function(next) {
  await this.model('Comment').deleteMany({ product: this._id });
  next();
});

module.exports = mongoose.model('Product', ProductSchema);