const mongoose = require('mongoose');

const OptionSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 100,
        unique: 1
    },
});

module.exports = mongoose.model('Option', OptionSchema);