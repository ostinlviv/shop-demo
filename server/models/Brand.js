const mongoose = require('mongoose');

const BrandSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 100,
        unique: 1
    },
});

module.exports = mongoose.model('Brand', BrandSchema);