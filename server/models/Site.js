const mongoose = require('mongoose');

const SiteSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 100,
        unique: 1
    },
    featured: {
        required: true,
        type: Array,
        default: []
    },
    siteInfo: {
        required: true,
        type: Array,
        default: []
    }
});

module.exports = mongoose.model('Site', SiteSchema);